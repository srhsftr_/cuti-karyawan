<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    public $timestamps = false;

    protected $table = "karyawan";
    protected $primarykey = "id";
    protected $fillable = [
        'id',
        'nama_karyawan',
        'nik',
        'tgl_masuk',
        'departemen',
        'jabatan',
        'status',
        'jumlah_cuti',
    ];

    public function cuti()
    {
        # code...
    }
}
