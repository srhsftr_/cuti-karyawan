<?php

namespace App\Imports;

use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class KaryawanImport implements ToModel, WithValidation, WithHeadingRow
{
    public function model(array $col)
    {
        return new User([
            'name'  => $col['nama'],
            'email' => $col['email'],
            'position' => $col['posisi'],
            'jumlah_cuti' => $col['jumlah_cuti'],
            'password' => Hash::make('password'),
        ]);
    }


    public function rules():array {
        return [
            'email' => ['required', Rule::unique('users')],
        ];
    }


    public function customValidationMessages() {
        return [
            'email.unique' => 'Huhu email duplikasi di file excel kamuu :('
        ];
    }
}
