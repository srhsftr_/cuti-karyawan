<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToArray;

class KaryawanExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $header = ['nama', 'email', 'posisi', 'jumlah_cuti'];
        $data   = User::select('name', 'email', 'position', 'jumlah_cuti')->get()->toArray();
        array_unshift($data, $header);

        return collect($data);
    }
}
