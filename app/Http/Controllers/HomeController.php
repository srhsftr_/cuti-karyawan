<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\{
    Karyawan,
    Cuti,
    User };

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $dates = Cuti::all();
        $user = auth()->user();
        $users = User::count();
        $total = json_decode(collect([
            'employee' => User::where('role', 'user')->count(),
            'cuti' => auth()->user()->jumlah_cuti,
        ]));
        $widget = [
            'users' => $users,
            //...
        ];


        if ($user->role === 'admin')  $cutis = Cuti::orderBy('created_at', 'desc')->limit(5)->get();
        else $cutis = Cuti::where('user_id', $user->id)->orderBy('created_at', 'desc')->limit(5)->get();

        $users = User::all();

        // data untuk yang sedang cuti
        $status= DB::table('cuti')->where("Status",2) ->orderBy("ID", "asc") ->get();
        
        $date = Cuti::select("cuti.*")
                ->whereRaw('? between start_date and finish_date', [date('Y-m-d')])
                ->where("Status",2) ->orderBy("ID", "asc")
                ->get();

        //Cuti::where('start_date', '<', 1)->where('finish_date', '>', 1)->get();
        //$date = Cuti::whereDate('start_date', Carbon::today())->get();

        return view('home', compact('widget', 'total', 'cutis', 'users', 'dates', 'date', 'status'));
    }

}
