<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CutiExport;
use RealRashid\SweetAlert\Facades\Alert;
use App\{Cuti, User};

class CutiController extends Controller {
    public function index() {
        $cutis = Cuti::all();
        $users = User::all();
        $user = auth()->user();

        if ($user->role === 'admin')  $cutis = Cuti::orderBy('created_at', 'desc')->paginate(10);
        else $cutis = Cuti::where('user_id', $user->id)->orderBy('created_at', 'desc')->paginate(10);
        return view('cuti.index', compact('cutis', 'users'));
    }


    public function export(Request $request) {
        return Excel::download(new CutiExport, 'datacuti.xlsx');
    }


    public function create() {
        $id = auth()->user()->id;
        $user = User::find($id);

        return view('cuti.create', compact('user'));
    }


    public function store(Request $request) {
        //insert data
            Cuti::create([
                'user_id' => auth()->user()->id,
                'start_date' =>  $request->start_date,
                'finish_date' => $request->finish_date,
                'jumlah_cuti' => $request->jumlah_cuti,
                'keterangan' => $request->keterangan,
            ]);

            //update data

            $user = User::find(auth()->user()->id);
            $data = Cuti::where('user_id', auth()->user()->id)->first();
            $status_sekarang = $data->status;
            if( $status_sekarang == 1 ){
                $user->jumlah_cuti = (int)$user->jumlah_cuti - (int)$request->jumlah_cuti;
                $user->update();
            }

            Alert::success('Success', 'Data berhasil ditambahkan');
            return redirect('cuti');

    }


    public function show($id) {
        //
    }


    public function edit($id) {
        //
    }



    public function destroy($id) {
        $deletecuti = Cuti::findorfail($id);
        $deletecuti->delete();

        Alert::success('Success', 'Data berhasil dihapus');
        return redirect()->back();
    }

    public function konfirmasi($id){
        alert()->question('Are you sure?', 'You won\'t be able to revert this!')
        ->showConfirmButton('<a href="/cuti/' .$id. '/delete" class="text-white">Delete</a>', '#ff0000')->toHtml()
        ->showCancelButton('Cancel', '#aaa')->reverseButtons();

        return redirect('/cuti');
    }


    public function approve($id) {
        $data = Cuti::find($id);
        Cuti::find($id)->update([
            'status'=>2
        ]);

        Alert::success('Success', 'Status berhasil diubah');
        return redirect()->back();
    }


    public function reject($id) {
        $data = Cuti::find($id);
        Cuti::find($id)->update([
            'status'=>1
        ]);

        Alert::success('Success', 'Status berhasil diubah');
        return redirect()->back();
    }


    public function search(Request $request) {
        $keyword = $request->keyword;
        $users   = User::where('name', 'LIKE', "%$keyword%")->get();
        $cutis   = collect([]);

        if(count($users) > 0) {
            $users->each(function($item, $key) use($cutis) {
                $cutiData = Cuti::where('user_id', $item->id)->first();
                if(!$cutiData) return;
                $cutis->push($cutiData);
            });
        } else {
            $users = User::all();
            $cutis = Cuti::where('keterangan', 'LIKE', "%$keyword%")->get();
        }

        return view('cuti.index', compact('cutis', 'keyword', 'users'));
    }


}
