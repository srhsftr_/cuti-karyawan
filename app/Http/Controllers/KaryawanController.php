<?php

namespace App\Http\Controllers;

use App\{Karyawan, User, Cuti};
use App\Imports\KaryawanImport;
use App\Exports\KaryawanExport;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class KaryawanController extends Controller {

    public function index() {
        $karyawans = User::where('role', 'user')->orderBy('name')->paginate(request()->per_page ?? 10);
        return view('karyawan.index', compact('karyawans'));
    }


    public function create() {

        return view('karyawan.create');
    }


    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'position' => ['required', 'string', 'max:20'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
        ])->validate(
            [
                'email.unique' => Alert::warning('Warning Title', 'Warning Message')
            ]
        );

        // if ($request->session()->has('error 1062') && config('sweetalert.middleware.auto_display_error_messages')) {
        //     $error = $request->session()->get('error 1062');

        //     if (!is_string($error)) {
        //         $error = $this->getErrors($error->getMessages());
        //     }

        //     alert()->error($error);
        // }

        $name = explode(' ', $request['name'], 2);
        $first_name = $name[0];
        $last_name = isset($name[1]) ? $name[1] : '';

        User::create([
            'name' => $first_name,
            'last_name' => $last_name,
            'email' => $request->email,
            'password' => Hash::make('password'),
            'jumlah_cuti'=> $request->jumlah_cuti,
            'position' => $request->position,
            'role' => 'admin',
        ]);

        Alert::success('Success', 'Data berhasil ditambahkan');
        return redirect('/karyawan');
    }

    public function karyawanimportexcel(Request $request){
        try{
            $file = $request->file('file');
            $namaFile = $file->getClientOriginalName();
            $file->move('DataKaryawan', $namaFile);
            Excel::import(new KaryawanImport, public_path('/DataKaryawan/'.$namaFile));
            Alert::success('Success', 'Data berhasil di import');
            return back();
            
        } catch(Exception $err) {
            $errMsg = $err->failures()[0]->errors()[0];
            Alert::error('Error', $errMsg);
            return back();
        }

        // $file->move('DataKaryawan', $namaFile);
        // return redirect()->back();
    }

    public function exportKaryawan(Request $request)
    {
        return Excel::download(new KaryawanExport, 'karyawan.xlsx');
    }

    public function show($id) {
        //
    }


    public function edit($id) {
        $item = User::findorfail($id);

        return view('karyawan.edit', compact('item'));
    }


    public function update(Request $request, $id) {
        $item = User::findorfail($id);
        $item->update($request->all());

        return redirect('/karyawan');
    }


    public function destroy($id) {
        $deletekaryawan = User::findorfail($id);

        $deletekaryawan->cuti()->delete();

        $deletekaryawan->delete();

        Alert::success('Success', 'Data berhasil dihapus');
        return redirect('/karyawan');
    }

    public function konfirmasi($id){
        alert()->question('Are you sure?', 'You won\'t be able to revert this!')
        ->showConfirmButton('<a href="/karyawan/' .$id. '/delete" class="text-white">Delete</a>', '#ff0000')->toHtml()
        ->showCancelButton('Cancel', '#aaa')->reverseButtons();

        return redirect('/karyawan');
    }

    public function deleteCheckedEmployees(Request $request) {
        $idk = $request->idk;
        Cuti::whereIn('user_id', $idk)->delete();
        User::whereIn('id',$idk)->delete();

        return response()->json(['success'=>"Employee have been deleted!"]);
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $karyawans = User::where('name', 'like', "%" . $keyword . "%")
                -> orWhere('email', 'like', "%". $keyword . "%")
                -> orWhere('position', 'like', "%". $keyword . "%")
                -> paginate($request->per_page ?? 10)
                -> appends(['keyword' => $request->keyword, 'per_page' => $request->per_page ?? 10]);


        return view('karyawan.index', compact('karyawans', 'keyword'));
    }
}
