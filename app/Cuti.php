<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    public $timestamps = true;

    protected $table = "cuti";
    protected $primarykey = "id";
    protected $fillable = [
        'id',
        'user_id',
        'start_date',
        'finish_date',
        'jumlah_cuti',
        'keterangan',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
