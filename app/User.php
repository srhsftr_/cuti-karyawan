<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Cuti;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'password', 'position', 'jumlah_cuti',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute()
    {
        if (is_null($this->last_name)) {
            return "{$this->name}";
        }

        return "{$this->name} {$this->last_name}";
    }

    public function cuti()
    {
        return $this->hasMany(Cuti::class);
    }


    public function sisaCuti() {
        /*
            0 -> Pending
            1 -> Rejected
            2 -> Approved
        */
        $cutiApproved = Cuti::all()->where('user_id', $this->id)->where('status', 2);
        $sisaCuti = $this->jumlah_cuti;

        foreach($cutiApproved as $item) {
            $sisaCuti -= $item->jumlah_cuti;
        }

        return $sisaCuti;
    }
}
