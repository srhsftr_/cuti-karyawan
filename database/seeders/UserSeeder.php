<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'last_name' => 'A',
            'email' => 'admin@gmail.com',
            'email_verified_at' => '2021-07-01',
            'password' => Hash::make('password'),
            'position' => 'admin',
            'jumlah_cuti' => '12',
            'role' => 'admin'
        ]);
    }
}
