@extends('layouts.admin')

@section('main-content')

<!-- Content Row -->
<div class="row">



    <!-- Content Column -->
    <div class="col-lg-12 mb-4">

        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Data {{ $item->name .' '. $item->last_name }}</h6>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form action="{{url('update/karyawan', $item)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">
                                Nama Karyawan
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" name="name" id="name"
                                class="form-control" placeholder="Nama Karyawan" value="{{ $item->name .' '. $item->last_name }}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="email">
                                Email
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="email" name="email" id="email"
                                class="form-control" placeholder="Email" value="{{$item->email}}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="jumlah_cuti">
                                Jatah Cuti
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="number" name="jumlah_cuti" id="jumlah_cuti"
                                class="form-control" placeholder="Jumlah Cuti" value="{{$item->jumlah_cuti}}" required>
                        </div>
                        <div class="form-group">
                            <label for="position">
                                Jabatan
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" name="position" id="position"
                                class="form-control" placeholder="Jabatan" value="{{$item->position}}" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
