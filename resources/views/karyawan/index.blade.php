@extends('layouts.admin')

@section('main-content')
<!-- Content Row -->
<div class="row">
    <!-- Content Column -->
    <div class="col-lg-12 mb-4">

        <!-- Project Card Example -->
        <div class="card shadow mb-4 card-responsive">

            <div class="container-fluid">
                <div class="card-header py-3 d-flex align-items-center">
                    <h6 class="m-0 font-weight-bold text-primary">Daftar Karyawan</h6>
                    @if(request()->user()->role == 'admin')
                        <button href="#" class="d-none d-sm-inline-block btn btn-primary ml-4" data-toggle="modal" data-target="#exampleModal">
                            <i class="fas fa-download"></i> Import</button>
                        <a href="{{route('exportkaryawan')}}" class="d-none d-sm-inline-block btn btn-success ml-2" data-toggle="collection" data-target="#exampleModal">
                            <i class="fas fa-upload"></i> Export</a>
                    @endif
                        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0" method="get" action="{{route('karyawan/search')}}">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="keyword" value="{{$keyword ?? ''}}" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                </div>
            </div>


            <div class="card-body table-responsive">
                <div class="mb-3 d-flex">
                <div class="dropdown mr-1">
                    <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-table mr-1"></i> {{ request()->per_page ?? 10 }}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item per-page" href="#">10</a>
                        <a class="dropdown-item per-page" href="#">25</a>
                        <a class="dropdown-item per-page" href="#">50</a>
                        <a class="dropdown-item per-page" href="#">100</a>
                    </div>
                    </div>
                    <a href="#" class="btn btn-danger disable" id="deleteAllSelectedRecord"><i class="fas fa-trash"></i></a>
                </div>

                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="chkCheckAll"/> </th>
                            <th scope="col">Nama</th>
                            <th scope="col">Email</th>
                            <th scope="col">Posisi</th>
                            <th scope="col" style="white-space: nowrap">Jatah Cuti</th>
                            <th scope="col" style="white-space: nowrap">Sisa Cuti</th>
                            <th scope="col" colspan="2" class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($karyawans as $item)
                            <tr id="kid{{$item->id}}" class="table">
                                <td>
                                    <input type="checkbox" name="idk" class="checkBoxClass" value="{{$item->id}}"/>
                                </td>
                                <td scope="row">{{ $item->name .' '. $item->last_name }}</td>
                                <td scope="row">{{ $item->email }}</td>
                                <td scope="row">{{ $item->position }}</td>
                                <td scope="row">{{ $item->jumlah_cuti }}</td>
                                <td scope="row">{{ $item->sisaCuti() }}</td>
                                <td style="width: 39px">
                                    {{-- <a href="{{ url('delete-karyawan', $item->id) }}" class="btn btn-danger swal-delete "> --}}
                                    <a href="/karyawan/{{$item->id}}/konfirmasi" class="btn btn-danger swal-delete ">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </td>
                                <td style="width: 39px">
                                    <a href="{{ url('edit-karyawan', $item->id)}}" class="btn btn-success ">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                {{$karyawans->links()}}
                <a class="btn btn-primary " href="{{ route('add-karyawan') }}">
                    <i class="fas fa-add"></i>Tambah Karyawan</a>
            </div>

            <div class="modal" id="exampleModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Import</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form action="{{route ('importkaryawan')}}" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="file" name="file" required="required">
                                </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                    </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(function(e){
            $('.per-page').each((key, el) => {
                el.onclick = () => {
                    const url = new URL(window.location.href);
                    url.searchParams.set('per_page', el.textContent);
                    window.location.href = url.href;
                }
            });

            $("#chkCheckAll").click(function(){
                $(".checkBoxClass").prop('checked', $(this).prop('checked'));
            })

            function checkAvailableToDelete() {
                const checkboxes = $("input:checkbox[name=idk]").length;
                const checked = $("input:checkbox[name=idk]:checked").length;

                
                if(checked > 0){
                    $("#deleteAllSelectedRecord").removeClass('disabled');
                    $("#chkCheckAll").prop('checked', false)
                } else
                    $("#deleteAllSelectedRecord").addClass('disabled');
                
                if(checkboxes === checked)
                    $("#chkCheckAll").prop('checked', true);
            }

            checkAvailableToDelete();
            $('input:checkbox[name=idk]').click(checkAvailableToDelete);            
            $('#chkCheckAll').click(checkAvailableToDelete);            


            $("#deleteAllSelectedRecord").click(function(e){
                e.preventDefault();
                var allidk = [];

                $("input:checkbox[name=idk]:checked").each(function(){
                    allidk.push($(this).val());
                })

                $.ajax({
                    url:"{{route('employee.deleteSelected')}}",
                    type:"DELETE",
                    data:{
                        _token:$("input[name=_token]").val(),
                        idk:allidk
                    },
                    success:function(response){
                        $.each(allidk,function(key,val){
                            $("#kid"+val).remove();
                        })
                    }
                });
            })
        });
    </script>
@endsection


