@extends('layouts.admin')
@section('main-content')

<!-- Content Row -->
<div class="row">

    @if(request()->user()->role == 'admin')
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div  class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                <a class="Nav-link" href="{{route('karyawan') }}">
                                <span> Jumlah Karyawan </span>
                                </a>
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $total->employee }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-users fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                Jatah Cuti Karyawan</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">12</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Menunggu Tanggapan</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $cutis->where('status', 0)->count() }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clock fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                Sedang Cuti</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $date->count() }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-umbrella-beach fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


            <!-- Content Column -->


                <!-- Project Card Example -->
            <tbody>
                <tr>
                    <td>
                        <!-- Content Row -->
                        <div class="row">
                            <div class="col-lg-6 mb-4">
                                <div class="card shadow mb-4">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 p-0 font-weight-bold text-primary nav-link" >
                                            <a href="{{route('cuti')}}">Data Cuti</a></h6>
                                    </div>
                                    <div class="card-body">
                                        <div class="card-body table-responsive p-0" style="min-height: 400px">
                                            <table class="table table-hover " id="dataTable" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>Nama</th>
                                                        <th>Mulai</th>
                                                        <th>Hingga</th>
                                                        <th style="white-space: nowrap">Sisa Cuti</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($cutis as $item)
                                                        @php
                                                            $user = $item->user;
                                                            $name = $user->name .' '. $user->last_name;
                                                        @endphp
                                                        <tr>
                                                            <td>{{ $name }}</td>
                                                            <td>{{ $item->start_date }}</td>
                                                            <td>{{ $item->finish_date }}</td>
                                                            <td>{{ $user->sisaCuti() }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>

                    <td>
                        <div class="col-lg-6 mb-4">

                            <!-- Illustrations -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 p-0 font-weight-bold text-primary nav-link" >
                                        <a href="{{route('cuti')}}">Karyawan Sedang Cuti</a></h6>
                                </div>
                                <div class="card-body">
                                    <div class="card-body table-responsive p-0" style="min-height: 400px">
                                        <table class="table table-hover " id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>Mulai</th>
                                                    <th>Hingga</th>
                                                    <th style="white-space: nowrap">Sisa Cuti</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($date as $item)
                                                    @php
                                                        $user = $item->user;
                                                        $name = $user->name .' '. $user->last_name;
                                                    @endphp
                                                    <tr>
                                                        <td>{{ $name }}</td>
                                                        <td>{{ $item->start_date }}</td>
                                                        <td>{{ $item->finish_date }}</td>
                                                        <td>{{ $user->sisaCuti() }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                </div>

                            </div>

                        </div>
                    </td>
                </tr>
            </tbody>


        </div>
    @endif



    <!-- Earnings (Monthly) Card Example -->
    @if (request()->user()->role == 'user')
    @php
        $user = auth()->user();
    @endphp
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                            Jatah Cuti Anda</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $user->jumlah_cuti }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                            <span> Sisa Cuti Anda</span>
                        </div>
                        <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ $user->sisaCuti() }}</div>
                            </div>
                            <div class="col">
                            </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                            <a href="{{route('cuti')}}">
                                <span class="text-success"> Cuti Digunakan </span>
                            </a>
                        </div>
                        <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ $user->jumlah_cuti - $user->sisaCuti() }}</div>
                            </div>
                            <div class="col">
                            </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-check fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                            Cuti Ditolak
                        </div>
                        <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ $cutis->where('status', 1)->count() }}</div>
                            </div>
                            <div class="col">
                            </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-ban fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="row">

        <!-- Content Column -->
        <div class="col-lg-12 mb-4">

            <!-- Project Card Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 p-0 font-weight-bold text-primary">
                        <a href="{{route('cuti')}}">Data Cuti Anda</a></h6>
                </div>
                <div class="card-body">
                    <div class="card-body table-responsive p-0" style="min-height: 400px">
                        <table class="table table-hover " id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mulai</th>
                                    <th>Hingga</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cutis as $item)
                                    @php
                                        $status;
                                        $statusClass = 'font-weight-bold';
                                        $user = $users->find($item->user_id);
                                        $name = $user->name .' '. $user->last_name;

                                        switch($item->status) {
                                            case 0:
                                                $status = 'Pending';
                                                $statusClass .= ' text-warning';
                                                break;
                                            case 1:
                                                $status = 'Rejected';
                                                $statusClass .= ' text-danger';
                                                break;
                                            case 2:
                                                $status = 'Approved';
                                                $statusClass .= ' text-success';
                                                break;
                                        }
                                    @endphp
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $item->start_date }}</td>
                                        <td>{{ $item->finish_date }}</td>
                                        <td>{{ $item->keterangan }}</td>
                                        <td class="{{ $statusClass }}">{{ $status }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
    @endif






@endsection
