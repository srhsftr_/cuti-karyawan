@extends('layouts.admin')

@section('main-content')
<!-- Content Row -->
<div class="row">

    <!-- Content Column -->
    <div class="col-lg-12 mb-4">

        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Ajukan Cuti</h6>
            </div>
            <div class="card-body">
                <div class="card-body table-responsive">
                    <form action="{{url('save-cuti')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">
                                Nama Karyawan
                                <span class="text-danger"> *</span>
                            </label>
                            <input name="name" id="name" value="{{$user->name .' '. $user->last_name}}" class="form-control" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="start_date">
                                Cuti Dari
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="date" name="start_date" id="start_date" class="form-control" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="finish_date">
                                Cuti Sampai
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="date" name="finish_date" id="finish_date" class="form-control" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="Jumlah Cuti">
                                Jumlah Cuti
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="number" name="jumlah_cuti" id="jumlah_cuti" rows="3"
                                class="form-control" placeholder="Jumlah Cuti" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="keterangan">
                                Keterangan
                                <span class="text-danger"> *</span>
                            </label>
                            <textarea type="text" name="keterangan" id="keterangan" rows="3"
                                class="form-control" placeholder="Keterangan Cuti" value="" required></textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Tambah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
