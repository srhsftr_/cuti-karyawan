@extends('layouts.admin')

@section('main-content')

<!-- Content Column -->
<div class="col-lg-12 mb-4 p-0">

    <!-- Project Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">Data Cuti</h6>
            @if(request()->user()->role == 'admin')
            <a href="{{route('exportcuti')}}" class="d-none d-sm-inline-block btn btn-success ml-4" data-toggle="collection" data-target="#exampleModal">
                <i class="fas fa-upload"></i> Export</a>
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0" method="get" action="{{route('cuti/search')}}">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" name="keyword" value="{{$keyword ?? ''}}" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            @endif
            @if(request()->user()->role == 'user')
              <a class="btn btn-primary ml-4" href="{{ route('add-cuti') }}">Ajukan Cuti</a>
            @endif
        </div>
        <div class="card-body table-responsive"  style="min-height: 400px">
            <table class="table table-hover " id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>

                        @if(request()->user()->role == 'admin')
                        <th>Nama</th>
                        @endif

                        <th>Mulai</th>
                        <th>Hingga</th>

                        @if(request()->user()->role == 'admin')
                        <th style="white-space: nowrap">Sisa Cuti</th>
                        <th style="white-space: nowrap">Jumlah Cuti</th>
                        @endif

                        <th>Keterangan</th>
                        <th>Status</th>

                        @if(request()->user()->role == 'user')
                        <th class="text-center">Aksi</th>
                        @endif

                        @if(request()->user()->role == 'admin')
                        <th class="text-center" colspan="2">Aksi</th>
                        @endif

                    </tr>
                </thead>
                <tbody>
                    @isset($cutis, $users)
                    @foreach ($cutis as $item)
                        @php
                            $status;
                            $statusClass = 'font-weight-bold';
                            $user = $users->find($item->user_id);
                            $name = $user->name .' '. $user->last_name;

                            switch($item->status) {
                                case 0:
                                    $status = 'Pending';
                                    $statusClass .= ' text-warning';
                                    break;
                                case 1:
                                    $status = 'Rejected';
                                    $statusClass .= ' text-danger';
                                    break;
                                case 2:
                                    $status = 'Approved';
                                    $statusClass .= ' text-success';
                                    break;
                            }
                        @endphp
                        <tr>

                            @if(request()->user()->role == 'admin')
                            <td>{{ $name }}</td>
                            @endif

                            <td>{{ $item->start_date }}</td>
                            <td>{{ $item->finish_date }}</td>

                            @if(request()->user()->role == 'admin')
                            <td>{{ $user->sisaCuti() }}</td>
                            <td>{{ $item->jumlah_cuti }}</td>
                            @endif

                            <td>{{ $item->keterangan }}</td>
                            <td class="{{ $statusClass }}">{{ $status }}</td>

                            @if(request()->user()->role == 'admin')
                            <td style="white-space: nowrap; width: 82px">
                                @if($item->status == 0)
                                <a href="{{ url('cuti/'.$item->id.'/approve') }}" class="btn btn-success">
                                    <i class="fas fa-check"></i>
                                </a>
                                <a href="{{ url('cuti/'.$item->id.'/reject') }}" class="btn btn-danger">
                                    <i class="fas fa-ban"></i>
                                </a>
                                @elseif($item->status == 1)
                                <a href="{{ url('cuti/'.$item->id.'/approve') }}" class="btn btn-success w-100">Approve</a>
                                @else
                                <a href="{{ url('cuti/'.$item->id.'/reject') }}" class="btn btn-danger w-100">Reject</a>
                                @endif
                            </td>
                            @endif
                            <td>
                                <a href="/cuti/{{$item->id}}/konfirmasi" class="btn btn-danger swal-delete ">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    @endisset
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

