<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-image: linear-gradient(rgba(4,9,30,0.7),rgba(4,9,30,0.7)), url("img/37130.jpg") ;
                background-position: center;
                background-size: cover;
                position: relative;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            .text-box{
                width: 90%;
                color: #fff;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                text-align: Center;
                letter-spacing: .1rem;
            }
            .text-box h1{
                font-size: 62px;
            }
            .hero-btn{
                display: inline-block;
                text-decoration: none;
                color:#fff;
                border: 1px solid #fff;
                padding: 12px 34px;
                font-size: 16px;
                font-weight: 600;
                letter-spacing: .1rem;
                background: linear-gradient(rgba(4,9,30,0.7),rgba(4,9,30,0.7));
                position: relative;
                cursor: pointer;
            }
            .hero-btn:hover{
                background: none;
                color:#fff;
                    
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            
            
            <div class="text-box">
                <h1>CUTI KARYAWAN</h1>
                @if (Route::has('login'))
                    <div >
                        @auth
                            <a href="{{ url('/home') }}" class="hero-btn links">Home</a>
                        @else
                            <a href="{{ route('login') }}" class="hero-btn links">Login</a>
                            
                            @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="hero-btn links">Register</a>
                            @endif
                        @endauth
                    </div>
                @endif
            </div>
        </div>
    </body>
</html>
