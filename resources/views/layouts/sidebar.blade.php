<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">
  <!-- Sidebar - Brand -->

  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
    <img class="sidebar-brand-icon mx-3" src="{{asset('img/LOGO_INTEK.png')}}" alt="" width="30" height="30">
    <div class="sidebar-brand-text mx-3">Cuti Karyawan</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item {{ Nav::isRoute('home') }}">
      <a class="nav-link" href="{{ route('home') }}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>{{ __('Dashboard') }}</span></a>
  </li>

  <!-- Nav Item - Karyawan Collapse Menu -->
    @if(request()->user()->role == 'admin')
    <li class="nav-item {{ Nav::isRoute('karyawan') }}">
        <a class="nav-link" href="{{ route('karyawan') }}">
            <i class="fas fa-fw fa-user"></i>
            <span>Karyawan</span>
        </a>
        {{-- <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('karyawan') }}">Karyawan</a>
            </div>
        </div> --}}
    </li>
    @endif



  <!-- Nav Item - Cuti Collapse Menu -->
  <li class="nav-item {{ Nav::isRoute('cuti') }}">
      <a class="nav-link" href="{{ route('cuti') }}" >
          <i class="fas fa-fw fa-table"></i>
          <span>Cuti</span>
      </a>
      {{-- <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
          data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="{{ route('cuti') }}">Data Cuti</a>
          </div>
      </div> --}}
  </li>

  {{-- <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
      {{ __('Setting') }}
  </div> --}}

  <!-- Nav Item - Profile -->
  <li class="nav-item {{ Nav::isRoute('profile') }}">
      <a class="nav-link" href="{{ route('profile') }}">
          <i class="fas fa-fw fa-user"></i>
          <span>{{ __('Profile') }}</span>
      </a>
  </li>

  {{-- <!-- Nav Item - About -->
  <li class="nav-item {{ Nav::isRoute('about') }}">
      <a class="nav-link" href="{{ route('about') }}">
          <i class="fas fa-fw fa-cog"></i>
          <span>{{ __('About') }}</span>
      </a>
  </li> --}}

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
