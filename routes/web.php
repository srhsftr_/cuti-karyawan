<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    HomeController,
    KaryawanController,
    CutiController,
    ProfileController
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->middleware(['guest']);

Route::middleware(['auth'])->group(function() {
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
    Route::put('/profile', [ProfileController::class, 'update'])->name('profile.update');

    Route::get('/about', function () {
        return view('about');
    })->name('about');

    Route::get('/blank', function () {
        return view('blank');
    })->name('blank');

    Route::middleware(['admin'])->group(function () {
        Route::get('karyawan', [KaryawanController::class, 'index'])->name('karyawan');
        Route::get('add-karyawan', [KaryawanController::class, 'create'])->name('add-karyawan');
        Route::post('save-karyawan', [KaryawanController::class, 'store'])->name('save-karyawan');
        Route::get('edit-karyawan/{id}', [KaryawanController::class, 'edit'])->name('edit-karyawan');
        Route::post('update/karyawan/{id}', [KaryawanController::class, 'update'])->name('update/karyawan');
        Route::get('/karyawan/{id}/konfirmasi', [KaryawanController::class, 'konfirmasi']);
        Route::get('/karyawan/{id}/delete ', [KaryawanController::class, 'destroy'])->name('delete-karyawan');
        Route::get('/karyawan/search', [KaryawanController::class, 'search'])->name('karyawan/search');
        Route::post('/importkaryawan', [KaryawanController::class, 'karyawanimportexcel'])->name('importkaryawan');
        Route::get('/exportkaryawan', [KaryawanController::class, 'exportKaryawan'])->name('exportkaryawan');
        Route::delete('/selected-employees', [KaryawanController::class, 'deleteCheckedEmployees'])->name('employee.deleteSelected');
    });


    Route::get('/exportcuti', [CutiController::class, 'export'])->name('exportcuti');
    Route::get('/cuti', [CutiController::class, 'index'])->name('cuti');
    Route::get('cuti/{id}/delete', [CutiController::class, 'destroy'])->name('delete-cuti');
    Route::get('add-cuti', [CutiController::class, 'create'])->name('add-cuti');
    Route::post('save-cuti', [CutiController::class, 'store'])->name('save-cuti');
    Route::get('/cuti/{id}/approve', [CutiController::class, 'approve']);
    Route::get('/cuti/{id}/reject', [CutiController::class, 'reject']);
    Route::get('/cuti/{id}/konfirmasi', [CutiController::class, 'konfirmasi']);
    Route::get('/cuti/search', [CutiController::class, 'search'])->name('cuti/search');

});
